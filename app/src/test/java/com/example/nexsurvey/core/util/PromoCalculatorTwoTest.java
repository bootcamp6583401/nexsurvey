package com.example.nexsurvey.core.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class PromoCalculatorTwoTest {
    // generate a test for each branch of the switch statement and the exception
    @Test
    public void testCalculatePromoPromo1() {
        PromoCalculatorTwo promoCalculatorTwo = new PromoCalculatorTwo();
        double result = promoCalculatorTwo.calculatePromo(100, "PROMO1");
        assertEquals(90, result, 0.001);
    }

    @Test
    public void testCalculatePromoPromo2() {
        PromoCalculatorTwo promoCalculatorTwo = new PromoCalculatorTwo();
        double result = promoCalculatorTwo.calculatePromo(100, "PROMO2");
        assertEquals(80, result, 0.001);
    }

    @Test
    public void testCalculatePromoPromo3() {
        PromoCalculatorTwo promoCalculatorTwo = new PromoCalculatorTwo();
        double result = promoCalculatorTwo.calculatePromo(100, "PROMO3");
        assertEquals(70, result, 0.001);
    }

    @Test
    public void testCalculatePromoPromo4() {
        PromoCalculatorTwo promoCalculatorTwo = new PromoCalculatorTwo();
        double result = promoCalculatorTwo.calculatePromo(100, "PROMO4");
        assertEquals(50, result, 0.001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCalculatePromoInvalidPromoType() {
        PromoCalculatorTwo promoCalculatorTwo = new PromoCalculatorTwo();
        promoCalculatorTwo.calculatePromo(100, "INVALID");
    }




}
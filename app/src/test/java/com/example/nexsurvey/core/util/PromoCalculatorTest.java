package com.example.nexsurvey.core.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class PromoCalculatorTest {

    // create a test method for each promo type
    @Test
    public void testCalculatePromoDiscount10() {
        double baseTotal = 100.0;
        String promoType = "DISCOUNT_10";
        double expected = 90.0; // 10% discount
        double actual = PromoCalculator.calculatePromo(baseTotal, promoType);
        assertEquals(expected, actual, 0.0);
    }

    @Test
    public void testCalculatePromoFixedDiscount() {
        double baseTotal = 100.0;
        String promoType = "FIXED_DISCOUNT";
        double[] additionalParams = {10.0, 20.0, 30.0};
        double expected = 40.0; // sum of additional parameters
        double actual = PromoCalculator.calculatePromo(baseTotal, promoType, additionalParams);
        assertEquals(expected, actual, 0.0);
    }

    // copy the two tests above, but use indonesian currency
    @Test
    public void testCalculatePromoDiscount10IDR() {
        double baseTotal = 100000.0;
        String promoType = "DISCOUNT_10";
        double expected = 90000.0; // 10% discount
        double actual = PromoCalculator.calculatePromo(baseTotal, promoType);
        assertEquals(expected, actual, 0.0);
    }

    @Test
    public void testCalculatePromoFixedDiscountIDR() {
        double baseTotal = 100000.0;
        String promoType = "FIXED_DISCOUNT";
        double[] additionalParams = {10000.0, 20000.0, 30000.0};
        double expected = 40000.0; // sum of additional parameters
        double actual = PromoCalculator.calculatePromo(baseTotal, promoType, additionalParams);
        assertEquals(expected, actual, 0.0);
    }

    // test to make sure an exception is thrown when totalAfterPromo is zero or negative when using additional parameters
    @Test(expected = IllegalArgumentException.class)
    public void testCalculatePromoFixedDiscountZero() {
        double baseTotal = 100.0;
        String promoType = "FIXED_DISCOUNT";
        double[] additionalParams = {-100.0, -200.0, -300.0};
        PromoCalculator.calculatePromo(baseTotal, promoType, additionalParams);
    }

}
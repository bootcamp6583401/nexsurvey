package com.example.nexsurvey.modul.login;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.nexsurvey.modul.login.http.LoginDetail;
import com.example.nexsurvey.modul.login.http.LoginErrorResponse;
import com.example.nexsurvey.modul.login.http.LoginRequest;
import com.example.nexsurvey.modul.login.http.LoginResponse;
import com.example.nexsurvey.modul.login.http.LoginResponseData;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.StringReader;

import okhttp3.ResponseBody;
import retrofit2.Response;



@RunWith(MockitoJUnitRunner.class)
public class LoginTest {
    private LoginCallback loginCallback;

    // create @Before method
    @Before
    public void setUp() {
        loginCallback = mock(LoginCallback.class);
    }

    @After
    public void tearDown() {
        Mockito.reset(loginCallback);
    }

    @Test
    public void testFailure() {
        Response<LoginResponse> loginResponseResponse = mock(Response.class);
        ResponseBody loginErrorResponseResponse = mock(ResponseBody.class);
        when(loginResponseResponse.isSuccessful()).thenReturn(false);
        when(loginErrorResponseResponse.charStream()).thenReturn(new StringReader("{\"code\": 404, \"description\": \"Not Found\"}"));
        when(loginResponseResponse.errorBody()).thenReturn(loginErrorResponseResponse);

        LoginResponseHandler loginResponseHandler = new LoginResponseHandler(loginCallback, mock(Context.class));
        loginResponseHandler.handleResponse(loginResponseResponse);

        verify(loginCallback).onFailure("Not Found");
    }

    @Test
    public void testSuccess() {
        Response<LoginResponse> loginResponseResponse = mock(Response.class);
        SharedPreferences.Editor mockSharedPreferencesEditor = mock(SharedPreferences.Editor.class);
        Context mockContext = mock(Context.class);

        when(mockContext.getSharedPreferences("nex_survey", Context.MODE_PRIVATE)).thenReturn(mock(SharedPreferences.class));
        when(mockContext.getSharedPreferences("nex_survey", Context.MODE_PRIVATE).edit()).thenReturn(mockSharedPreferencesEditor);

        when(loginResponseResponse.isSuccessful()).thenReturn(true);

        LoginDetail loginDetail = new LoginDetail();
        loginDetail.setName("ansell");
        loginDetail.setAge(33);

        LoginResponseData loginResponseData = new LoginResponseData();
        loginResponseData.setToken("token");

        loginResponseData.setLoginDetail(loginDetail);

        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setLoginResponseData(loginResponseData);

        loginResponse.setCode(200);
        when(loginResponseResponse.body()).thenReturn(loginResponse);

        LoginResponseHandler loginResponseHandler = new LoginResponseHandler(loginCallback, mockContext);
        loginResponseHandler.handleResponse(loginResponseResponse);

        verify(loginCallback).onSuccess(loginResponse);
    }

}

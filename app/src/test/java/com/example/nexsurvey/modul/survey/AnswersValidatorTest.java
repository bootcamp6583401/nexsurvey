package com.example.nexsurvey.modul.survey;


import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.example.nexsurvey.modul.survey.http.Question;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

public class AnswersValidatorTest {

    // create mockito mock for TypedGenericCallback
    private TypedGenericCallback<ArrayList<QuestionAnswer>> onSaveCallback;

    // create @Before method to initialize onSaveCallback
    @Before
    public void setUp() {
        onSaveCallback = mock(TypedGenericCallback.class);
    }

    // create @After method to reset mocks
    @After
    public void tearDown() {
        reset(onSaveCallback);
    }

    // create test that verifies onSaveCallback.onFailure is called when there is a missing answer
    @Test
    public void testValidateAnswers_missingAnswer() {
        // create AnswersValidator instance
        AnswersValidator answersValidator = new AnswersValidator(onSaveCallback);
        // create QuestionAnswer instance
        QuestionAnswer questionAnswer = new QuestionAnswer("question", null, new ArrayList<>(), true);
        // create Question instance
        Question question = new Question();
        // set question attributes using setters
        question.setRequired(true);
        question.setAnswer(questionAnswer);
        // create ArrayList of Question
        ArrayList<Question> questions = new ArrayList<>();
        questions.add(question);
        // call validateAnswers
        answersValidator.validateAnswers(questions);
        // verify onSaveCallback.onFailure is called
        verify(onSaveCallback).onFailure(argThat((Throwable t) -> t.getMessage().equals(String.format("Question %d is missing an answer!", 1))));

    }

    // create test that verifies onSaveCallback.onSuccess is called when there are no missing answers
    @Test
    public void testValidateAnswers_noMissingAnswer() {
        // create AnswersValidator instance
        AnswersValidator answersValidator = new AnswersValidator(onSaveCallback);
        // create QuestionAnswer instance
        QuestionAnswer questionAnswer = new QuestionAnswer("question", "singleAnswer", new ArrayList<>(), true);
        // create Question instance
        Question question = new Question();
        // set question attributes using setters
        question.setRequired(true);
        question.setAnswer(questionAnswer);
        // create ArrayList of Question
        ArrayList<Question> questions = new ArrayList<>();
        questions.add(question);
        // call validateAnswers
        answersValidator.validateAnswers(questions);
        // verify onSaveCallback.onSuccess is called

        ArrayList<QuestionAnswer> answers = new ArrayList<>();
        answers.add(questionAnswer);
        verify(onSaveCallback).onSuccess(answers);
    }

    // test for when answer is not required but singleAnswer is empty
    @Test
    public void testValidateAnswers_singleAnswerEmpty() {
        // create AnswersValidator instance
        AnswersValidator answersValidator = new AnswersValidator(onSaveCallback);
        // create QuestionAnswer instance
        QuestionAnswer questionAnswer = new QuestionAnswer("question", "", new ArrayList<>(), false);
        // create Question instance
        Question question = new Question();
        // set question attributes using setters
        question.setRequired(false);
        question.setAnswer(questionAnswer);
        // create ArrayList of Question
        ArrayList<Question> questions = new ArrayList<>();
        questions.add(question);
        // call validateAnswers
        answersValidator.validateAnswers(questions);
        // verify onSaveCallback.onSuccess is called

        ArrayList<QuestionAnswer> answers = new ArrayList<>();
        answers.add(questionAnswer);
        verify(onSaveCallback).onSuccess(answers);
    }

    // Create a test case to verify that onSuccess is called when it's required,
    // singleAnswer is empty, but listAnswers is not empty
    @Test
    public void testValidateAnswers_singleAnswerEmpty_listAnswersNotEmpty() {
        // create AnswersValidator instance
        AnswersValidator answersValidator = new AnswersValidator(onSaveCallback);
        // create QuestionAnswer instance
        ArrayList<String> listAnswers = new ArrayList<>();
        listAnswers.add("listAnswer");
        QuestionAnswer questionAnswer = new QuestionAnswer("question", null, listAnswers, true);
        // create Question instance
        Question question = new Question();
        // set question attributes using setters
        question.setRequired(true);
        question.setAnswer(questionAnswer);
        // create ArrayList of Question
        ArrayList<Question> questions = new ArrayList<>();
        questions.add(question);
        // call validateAnswers
        answersValidator.validateAnswers(questions);
        // verify onSaveCallback.onSuccess is called

        ArrayList<QuestionAnswer> answers = new ArrayList<>();
        answers.add(questionAnswer);
        verify(onSaveCallback).onSuccess(answers);
    }

    // verify onSuccess when singleAnswer is null, listAnswers is empty, but imagePath is filled
    @Test
    public void testValidateAnswers_singleAnswerNull_listAnswersEmpty_imagePathFilled() {
        // create AnswersValidator instance
        AnswersValidator answersValidator = new AnswersValidator(onSaveCallback);
        // create QuestionAnswer instance
        QuestionAnswer questionAnswer = new QuestionAnswer("question", null, new ArrayList<>(), true);
        questionAnswer.setImagePath("imagePath");
        // create Question instance
        Question question = new Question();
        // set question attributes using setters
        question.setRequired(true);
        question.setAnswer(questionAnswer);
        // create ArrayList of Question
        ArrayList<Question> questions = new ArrayList<>();
        questions.add(question);
        // call validateAnswers
        answersValidator.validateAnswers(questions);
        // verify onSaveCallback.onSuccess is called

        ArrayList<QuestionAnswer> answers = new ArrayList<>();
        answers.add(questionAnswer);
        verify(onSaveCallback).onSuccess(answers);
    }

    // verify onFailure when there's two questions, but the second question is missing an answer while the first question is not
    @Test
    public void testValidateAnswers_missingAnswerSecondQuestion() {
        // create AnswersValidator instance
        AnswersValidator answersValidator = new AnswersValidator(onSaveCallback);
        // create QuestionAnswer instance
        QuestionAnswer questionAnswer = new QuestionAnswer("question", "singleAnswer", new ArrayList<>(), true);
        // create Question instance
        Question question = new Question();
        // set question attributes using setters
        question.setRequired(true);
        question.setAnswer(questionAnswer);
        // create QuestionAnswer instance
        QuestionAnswer questionAnswer2 = new QuestionAnswer("question2", null, new ArrayList<>(), true);
        // create Question instance
        Question question2 = new Question();
        // set question attributes using setters
        question2.setRequired(true);
        question2.setAnswer(questionAnswer2);
        // create ArrayList of Question
        ArrayList<Question> questions = new ArrayList<>();
        questions.add(question);
        questions.add(question2);
        // call validateAnswers
        answersValidator.validateAnswers(questions);
        // verify onSaveCallback.onFailure is called
        verify(onSaveCallback).onFailure(argThat((Throwable t) -> t.getMessage().equals(String.format("Question %d is missing an answer!", 2))));
    }


  
}
package com.example.nexsurvey;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import android.content.Context;

import androidx.test.core.app.ActivityScenario;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.ViewAssertion;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import com.example.nexsurvey.modul.landing.LandingView;
import com.example.nexsurvey.modul.login.LoginCallback;
import com.example.nexsurvey.modul.login.LoginRepo;
import com.example.nexsurvey.modul.login.http.LoginRequest;
import com.example.nexsurvey.modul.login.http.LoginResponse;

import java.util.Optional;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class ActivityTest {

    @Rule
    public ActivityScenarioRule<LandingView> activityScenarioRule = new ActivityScenarioRule<>(LandingView.class);


    @Test
    public void mainTest() {
        // Context of the app under test.
//        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
//
//        ActivityScenario<LandingView> scenario = activityScenarioRule.getScenario();

        // Click the button with the specified text "Get Started"
        onView(withText("Get Started"))
                .perform(ViewActions.click());

        onView(withText("Login"))
                .perform(ViewActions.click());

        onView(

//                withText(CoreMatchers.startsWith("Hey"))
                withId(R.id.cvSurveyReport)
        ) .perform(ViewActions.click());

//        Espresso.onView(withText(""))
    }




}
package com.example.nexsurvey.survey;

import android.content.Context;
import android.util.Log;

import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import com.example.nexsurvey.core.db.AppDatabase;
import com.example.nexsurvey.modul.survey.SurveyResponse;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.example.nexsurvey.modul.survey.db.QuestionAnswerDao;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;
import com.example.nexsurvey.modul.survey.db.SurveyResponseDao;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

@RunWith(AndroidJUnit4.class)
public class SurveyTest extends TestCase {

    private AppDatabase db;

    private SurveyResponseDao surveyResponseDao;

    private QuestionAnswerDao questionAnswerDao;

    @Before
    @Override
    public void setUp() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        db = Room.inMemoryDatabaseBuilder(appContext, AppDatabase.class).build();

        surveyResponseDao = db.responseDao();
        questionAnswerDao = db.answerDao();
    }

    @After
    public  void closeDb() {
        db.close();
    }

    @Test
    public void insertManyAndGet(){

        ArrayList<SurveyCorrespondentResponse> responses = new ArrayList<>();

        SurveyCorrespondentResponse response1 = new SurveyCorrespondentResponse();
        response1.setName("Name");
        response1.setAge(33);
        response1.setAnswers(new ArrayList<>());
        response1.setSurveyTitle("Survey Title");
        response1.setSurveyDescription("Description");


        responses.add(response1);
        surveyResponseDao.insertAll(responses);

        List<SurveyCorrespondentResponse> savedResponses = surveyResponseDao.getAll();

        assertEquals(1, savedResponses.size());
    }

    @Test
    public void insertAndGetById(){

        SurveyCorrespondentResponse response = new SurveyCorrespondentResponse();
        response.setName("Name");
        response.setAge(33);
        response.setAnswers(new ArrayList<>());
        response.setSurveyTitle("Survey Title");
        response.setSurveyDescription("Description");


        long id = surveyResponseDao.insert(response);

        SurveyCorrespondentResponse savedResponse = surveyResponseDao.loadById(((Long)id).intValue());

        assertEquals(33, savedResponse.getAge());
    }

    @Test
    public void checkListConverter(){

        List<QuestionAnswer> questionAnswers = new ArrayList<>();

        ArrayList<String> listAnswers = new ArrayList<>();
        listAnswers.add("1");
        listAnswers.add("2");
        listAnswers.add("3");

        questionAnswers.add(new QuestionAnswer("Question 1", "Single Answer", new ArrayList<>(), false));
        questionAnswers.add(new QuestionAnswer("Question 2", null, listAnswers, false));

        SurveyCorrespondentResponse response = new SurveyCorrespondentResponse();
        response.setName("Name");
        response.setAge(33);
        response.setAnswers(questionAnswers);
        response.setSurveyTitle("Survey Title");
        response.setSurveyDescription("Description");


        long id = surveyResponseDao.insert(response);

        int intId = ((Long)id).intValue();

        SurveyCorrespondentResponse savedResponse = surveyResponseDao.loadById(intId);
        questionAnswers.get(0).setResponseId(intId);
        questionAnswers.get(1).setResponseId(intId);
        questionAnswerDao.insertAll(new ArrayList<>(questionAnswers));

        List<QuestionAnswer> savedAnswers = questionAnswerDao.loadByResponseId(intId);

        assertEquals("Single Answer", savedAnswers.get(0).getSingleAnswer());
        assertEquals(3, savedAnswers.get(1).getListAnswers().size());
    }
}

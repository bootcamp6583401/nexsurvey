package com.example.nexsurvey.survey;


import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import com.example.nexsurvey.core.db.AppDatabase;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.example.nexsurvey.modul.survey.db.QuestionAnswerDao;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;
import com.example.nexsurvey.modul.survey.db.SurveyResponseDao;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

@RunWith(AndroidJUnit4.class)
public class SurveyGithubCopilotTest extends TestCase {

    // declare a variable of type AppDatabase
    private AppDatabase db;

    // declare a variable of type SurveyResponseDao
    private SurveyResponseDao surveyResponseDao;

    // declare a variable of type QuestionAnswerDao
    private QuestionAnswerDao questionAnswerDao;

    // Create a setup method that initializes the database and the SurveyResponseDao
    @Before
    @Override
    public void setUp() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        db = AppDatabase.getInstance(appContext);
        surveyResponseDao = db.responseDao();
        questionAnswerDao = db.answerDao();
    }

    // create a close @After method that closes the database
    @After
    public void closeDb() {
        db.close();
    }

    // create a test method that inserts a SurveyCorrespondentResponse object into the database, retrieves it and asserts its values
    @Test
    public void insertAndGetSurveyCorrespondentResponse() {
        SurveyCorrespondentResponse response = new SurveyCorrespondentResponse();
        response.setName("John Doe");
        response.setAge(30);
        response.setSurveyTitle("Survey Title");
        response.setAnswers(new ArrayList<>());

        // insert the response and get its id, then fetch it using the loadById method from the dao
        long id = surveyResponseDao.insert(response);

        // fetch it but first cast the id into an int
        SurveyCorrespondentResponse retrievedResponse = surveyResponseDao.loadById((int) id);


        assertEquals(response.getName(), retrievedResponse.getName());
        assertEquals(response.getAge(), retrievedResponse.getAge());
        assertEquals(response.getSurveyTitle(), retrievedResponse.getSurveyTitle());
    }

    // create a test that insert a list of QuestionAnswer objects into the database and assert the total number of items in the list
    @Test
    public void insertManyAndGet() {
        ArrayList<QuestionAnswer> answers = new ArrayList<>();

        QuestionAnswer answer1 = new QuestionAnswer("Question 1", "Answer 1", new ArrayList<>(), true);
        QuestionAnswer answer2 = new QuestionAnswer("Question 2", "Answer 2", new ArrayList<>(), true);

        answers.add(answer1);
        answers.add(answer2);

        questionAnswerDao.insertAll(answers);

        List<QuestionAnswer> savedAnswers = questionAnswerDao.getAll();

        assertEquals(2, savedAnswers.size());
    }

}
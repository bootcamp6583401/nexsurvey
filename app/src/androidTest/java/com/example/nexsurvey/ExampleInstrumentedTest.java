package com.example.nexsurvey;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import com.example.nexsurvey.modul.login.LoginCallback;
import com.example.nexsurvey.modul.login.LoginRepo;
import com.example.nexsurvey.modul.login.http.LoginRequest;
import com.example.nexsurvey.modul.login.http.LoginResponse;

import java.util.Optional;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.example.nexsurvey", appContext.getPackageName());
    }

    @Test
    public void testCorrectCredentials(){
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        LoginRepo loginRepo = new LoginRepo(appContext);

        LoginRequest loginRequest = new LoginRequest("ansell", "Nexsoft!123");

        loginRepo.login(loginRequest, new LoginCallback() {
            @Override
            public void onSuccess(LoginResponse loginResponse) {
                assertEquals(200, loginResponse.getCode().intValue());
            }

            @Override
            public void onFailure(String description) {
                assertEquals(false,true);
            }
        });




    }

    @Test
    public void testIncorrectCredentials(){
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        LoginRepo loginRepo = new LoginRepo(appContext);

        LoginRequest loginRequest = new LoginRequest("assnsell", "Nexsoft!123");

        loginRepo.login(loginRequest, new LoginCallback() {
            @Override
            public void onSuccess(LoginResponse loginResponse) {
                assertEquals(true, false);
            }

            @Override
            public void onFailure(String description) {
                assertEquals("Something went wrong.",description);
            }
        });




    }


}





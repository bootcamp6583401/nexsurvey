package com.example.nexsurvey.core;

public interface TypedGenericCallback<T> {
    void onSuccess(T object);
    void onFailure(Throwable t);
}

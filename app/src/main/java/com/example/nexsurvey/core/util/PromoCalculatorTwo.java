package com.example.nexsurvey.core.util;

public class PromoCalculatorTwo {
    public double calculatePromo(double baseTotal, String promoType) throws IllegalArgumentException {
        switch (promoType) {
            case "PROMO1":
                return baseTotal * 0.9; // 10% discount
            case "PROMO2":
                return baseTotal * 0.8; // 20% discount
            case "PROMO3":
                return baseTotal * 0.7; // 30% discount
            case "PROMO4":
                return baseTotal * 0.5; // 50% discount
            default:
                throw new IllegalArgumentException("Invalid promo type: " + promoType);
        }
    }

}

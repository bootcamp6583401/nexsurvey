package com.example.nexsurvey.core.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.example.nexsurvey.modul.survey.db.QuestionAnswerDao;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;
import com.example.nexsurvey.modul.survey.db.SurveyResponseDao;
import com.example.nexsurvey.modul.tracking.UserLocation;
import com.example.nexsurvey.modul.tracking.UserLocationDao;


@Database(entities = {QuestionAnswer.class, SurveyCorrespondentResponse.class, UserLocation.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract QuestionAnswerDao answerDao();

    public abstract SurveyResponseDao responseDao();

    public abstract UserLocationDao locationDao();

    private static volatile AppDatabase instance = null;
    public static AppDatabase getInstance(Context context) {

        if (instance == null) {
            instance  = Room.databaseBuilder(context,
                    AppDatabase.class, "nexsurvey").build();
        }
        return instance;
    }
}
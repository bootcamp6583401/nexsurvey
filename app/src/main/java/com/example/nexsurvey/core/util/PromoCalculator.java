package com.example.nexsurvey.core.util;

public class PromoCalculator {

    public static double calculatePromo(double baseTotal, String promoType, double... additionalParams) {
        // Implement your promo calculation logic here based on promoType and additional parameters
        double promoAmount = 0.0;

        // Example: Apply a 10% discount if promoType is "DISCOUNT_10"
        if (promoType.equals("DISCOUNT_10")) {
            promoAmount = baseTotal * 0.1; // 10% discount
        }

        // Example: Apply a fixed discount based on sum of additional parameters
        if (promoType.equals("FIXED_DISCOUNT")) {
            for (double param : additionalParams) {
                promoAmount += param; // Sum of all additional parameters
            }
        }

        // Example: Apply other promo types and calculations as needed

        // throw appropriate error if the returned value is zero or negative
        if (promoAmount <= 0) {
            throw new IllegalArgumentException("Invalid promo calculation result");
        }

        return baseTotal - promoAmount;
    }


}
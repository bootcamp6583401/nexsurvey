package com.example.nexsurvey.core;

public interface GenericCallback {
    void onSuccess(Object object);
    void onFailure(Object object);
}

package com.example.nexsurvey.modul.survey.http;

import com.example.nexsurvey.modul.login.http.LoginRequest;
import com.example.nexsurvey.modul.login.http.LoginResponse;
import com.example.nexsurvey.modul.survey.SurveyResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface SurveyApi {
//    @Headers("Content-type: application/json")
//    @POST("login")
//    Call<LoginResponse> login(@Body LoginRequest loginRequest);

    @GET("questions")
    Call<SurveyListResponse> getSurveyList(@Header("Authorization") String token);

    @GET("questions")
    Call<SurveyResponse> getOneSurvey(@Header("Authorization") String token, @Query("page") int page);


}

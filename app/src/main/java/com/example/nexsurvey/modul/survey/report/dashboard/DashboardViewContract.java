package com.example.nexsurvey.modul.survey.report.dashboard;

import com.example.nexsurvey.modul.survey.db.QuestionAnswer;

import java.util.ArrayList;

public interface DashboardViewContract {
    void renderDashboard(ArrayList<QuestionAnswer> answers, int totalResponses, String currentUser, ArrayList<Summary> summaries);
}

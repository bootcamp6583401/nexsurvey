package com.example.nexsurvey.modul.survey.report_detail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nexsurvey.R;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;

import java.util.ArrayList;

public class AnswersListAdapter extends RecyclerView.Adapter<AnswersListAdapter.ViewHolder> {
    Context context;

    ArrayList<QuestionAnswer> answers;

    public void setAnswers(ArrayList<QuestionAnswer> answers){
        this.answers = answers;
        notifyDataSetChanged();
    }

    public AnswersListAdapter(Context context, ArrayList<QuestionAnswer> answers) {
        this.context = context;
        this.answers = answers;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.response_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        QuestionAnswer answer = answers.get(position);
        holder.multipleAnswersList.setVisibility(View.GONE);
        holder.singleAnswerText.setVisibility(View.GONE);
        holder.questionText.setText(answer.getQuestion());
        holder.image.setVisibility(View.GONE);

        if(answer.isRequired()){
            holder.questionText.setTextColor(ContextCompat.getColor(context, R.color.red));
        }

        if(answer.getSingleAnswer() != null){
            holder.singleAnswerText.setVisibility(View.VISIBLE);
            holder.singleAnswerText.setText(answer.getSingleAnswer());

        }else if(answer.getListAnswers().size() > 0){
            holder.multipleAnswersList.setVisibility(View.VISIBLE);

            ArrayAdapter<String> listAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, answer.getListAnswers());

            holder.multipleAnswersList.setAdapter(listAdapter);
        }else if(answer.getImageBitmap() != null ) {
            holder.image.setVisibility(View.VISIBLE);
            holder.image.setImageBitmap(answer.getImageBitmap());

        }

    }

    @Override
    public int getItemCount() {
        return answers.size();
    }

    public static class ViewHolder extends  RecyclerView.ViewHolder {
        TextView questionText, singleAnswerText;

        ListView multipleAnswersList;

        ImageView image;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            questionText = itemView.findViewById(R.id.tvQuestionText);
            singleAnswerText = itemView.findViewById(R.id.tvSingleAnswer);
            multipleAnswersList = itemView.findViewById(R.id.lvMultipleAnswers);
            image = itemView.findViewById(R.id.ivImage);
        }
    }
}

package com.example.nexsurvey.modul.tracking;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "user_locations")

public class UserLocation {
    @PrimaryKey(autoGenerate = true)
    private Integer id;

    private double latitude, longitude;

    private String username;

    public UserLocation() {
    }

    public UserLocation(double latitude, double longitude, String username) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.username = username;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

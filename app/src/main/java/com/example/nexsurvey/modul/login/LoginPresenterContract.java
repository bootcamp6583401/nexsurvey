package com.example.nexsurvey.modul.login;

import com.example.nexsurvey.modul.login.http.LoginRequest;

public interface LoginPresenterContract {
    void onLogin(LoginRequest loginRequest);

}

package com.example.nexsurvey.modul.survey;

import android.graphics.Bitmap;

import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;

import java.util.ArrayList;

public interface SurveyFormPresenterContract {
    void loadSurvey(int page);

    void onSaveAnswers(SurveyCorrespondentResponse surveyResponse, ArrayList<QuestionAnswer> answers);

    void saveImage(Bitmap imageBitmap);
}

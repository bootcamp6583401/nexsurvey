package com.example.nexsurvey.modul.survey.report;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nexsurvey.R;
import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.modul.survey.SurveyFormView;
import com.example.nexsurvey.modul.survey.adapter.QuestionListAdapter;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;
import com.example.nexsurvey.modul.survey.report.dashboard.DashboardView;
import com.example.nexsurvey.modul.survey.report_detail.ResponseDetailView;

import java.util.ArrayList;

public class SurveyReportView extends AppCompatActivity implements SurveyReportViewContract {

    RecyclerView surveyResponsesList;

    SurveyResponsesListAdapter responsesListAdapter;

    SurveyReportPresenterContract presenter;

    ImageView backBtn;

    ImageButton dashBtn;

    TextView title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.survey_report_view);

        surveyResponsesList = findViewById(R.id.rvSurveyResponses);

        dashBtn = findViewById(R.id.btnDashboard);

        backBtn = findViewById(R.id.btnBack);

        title = findViewById(R.id.tvSurveyPageTitle);

        presenter = new SurveyReportPresenter(this, new SurveyReportRepo(this));

        dashBtn.setOnClickListener(v -> {
            Intent dashIntent = new Intent(SurveyReportView.this, DashboardView.class);

            startActivity(dashIntent);
        });

//        title.setOnClickListener(v -> {});

        responsesListAdapter = new SurveyResponsesListAdapter(this, new ArrayList<>(), new TypedGenericCallback<Integer>() {
            @Override
            public void onSuccess(Integer object) {
                Intent detailPageIntent = new Intent(SurveyReportView.this, ResponseDetailView.class);
                detailPageIntent.putExtra("id", object);
                startActivity(detailPageIntent);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

        surveyResponsesList.setAdapter(responsesListAdapter);
        surveyResponsesList.setLayoutManager(new LinearLayoutManager(this));

        presenter.loadResponses();

        backBtn.setOnClickListener(v -> {
            finish();
        });
    }

    @Override
    public void renderResponses(ArrayList<SurveyCorrespondentResponse> responses) {
        responsesListAdapter.setResponses(responses);
    }
}
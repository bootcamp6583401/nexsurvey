package com.example.nexsurvey.modul.home;

import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.modul.survey.http.Survey;

import java.util.ArrayList;

public class HomePresenter implements HomePresenterContract{

    HomeViewContract view;
    HomeRepoContract repo;

    public HomePresenter(HomeViewContract view, HomeRepoContract repo){
        this.view = view;
        this.repo = repo;
    }

    @Override
    public void loadSurveys() {
        repo.getSurveys(new TypedGenericCallback<ArrayList<Survey>>() {
            @Override
            public void onSuccess(ArrayList<Survey> object) {
                view.renderSurveys(object);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
}

package com.example.nexsurvey.modul.tracking;

import android.content.Context;
import android.os.Looper;

import androidx.core.os.HandlerCompat;

import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.core.db.AppDatabase;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

public class LocationRepo implements LocationRepoContract{
    Context context;


    public LocationRepo(Context context) {
        this.context = context;
    }

    @Override
    public void getUserLocations(TypedGenericCallback<ArrayList<UserLocation>> userLocationsCallback) {
        Executors.newCachedThreadPool().execute(() -> {
            try {
                AppDatabase db = AppDatabase.getInstance(context);
                List<UserLocation> responses =  db.locationDao().getAll();

                HandlerCompat.createAsync(Looper.getMainLooper()).post(() -> userLocationsCallback.onSuccess(new ArrayList<>(responses)));

            }catch (Exception e){

            }
        });
    }
}

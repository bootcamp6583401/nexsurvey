package com.example.nexsurvey.modul.tracking;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.example.nexsurvey.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.example.nexsurvey.databinding.ActivityLocationViewBinding;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

public class LocationView extends AppCompatActivity implements OnMapReadyCallback, LocationViewContract {

    private GoogleMap mMap;
    private ActivityLocationViewBinding binding;

    private ArrayList<LatLng>  coords = new ArrayList<>();

    private LocationPresenterContract presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        binding = ActivityLocationViewBinding.inflate(getLayoutInflater());
        presenter = new LocationPresenter(this, new LocationRepo(this));
        setContentView(binding.getRoot());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        presenter.loadUserLocations();
    }

    @Override
    public void renderLocations(ArrayList<UserLocation> userLocations) {

        PolylineOptions polylineOptions = new PolylineOptions();
        LatLng prevLatLng = null;
        for (UserLocation userLocation :
                userLocations) {
            LatLng latLng = new LatLng(userLocation.getLatitude(), userLocation.getLongitude());
            coords.add(latLng);     
            polylineOptions.add(latLng);
            if(prevLatLng == null || (prevLatLng.latitude != latLng.latitude || prevLatLng.longitude != latLng.longitude)){
                mMap.addMarker(new MarkerOptions().position(latLng));
                prevLatLng = latLng;
            }
        }

        if(coords.size() > 0 && mMap != null){
            mMap.addPolyline(polylineOptions);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(coords.get(coords.size() - 1)));
            mMap.moveCamera(CameraUpdateFactory.zoomTo(mMap.getMaxZoomLevel()));
            Log.d("TAG", "renderLocations: RENDERING");
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
package com.example.nexsurvey.modul.survey;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nexsurvey.R;
import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.modul.home.HomeView;
import com.example.nexsurvey.modul.survey.adapter.QuestionListAdapter;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;
import com.example.nexsurvey.modul.survey.http.Survey;
import com.example.nexsurvey.modul.survey.report.SurveyReportView;
import com.example.nexsurvey.modul.survey.report_detail.ResponseDetailView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;

public class SurveyFormView extends AppCompatActivity implements SurveyFormViewContract{

    private static final String CHANNEL_ID = "BROSKI";
    private static final int NOTIFICATION_ID = 333;
    RecyclerView questionList;

    TextInputEditText nameTextInput, ageTextInput;

    QuestionListAdapter questionListAdapter;

    SurveyFormPresenterContract presenter;

    FloatingActionButton saveFab;

    Survey survey;

    ImageView backBtn;

    TextView surveyTitle;

    ProgressBar loader;

    ActivityResultLauncher<Intent> cameraResultLauncher;

    private static final int PERMISSION_REQUEST_CODE = 1002;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.survey_view);

        presenter = new SurveyFormPresenter(this, new SurveyFormRepo(this));

        questionList = findViewById(R.id.rvQuestionList);
        saveFab = findViewById(R.id.fabSave);
        loader = findViewById(R.id.pbLoader);
        nameTextInput = findViewById(R.id.etCorrespondentName);
        ageTextInput = findViewById(R.id.etAge);
        backBtn = findViewById(R.id.btnBack);
        surveyTitle = findViewById(R.id.tvSurveyPageTitle);

        cameraResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if(result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    if(data != null){
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
//                        questionListAdapter.notifyItemChanged(questionListAdapter.getCurrentCameraPos(), photo);
                        presenter.saveImage(photo);

                    }
                }
            }
        });

        backBtn.setOnClickListener(v -> {
            finish();
        });

        questionListAdapter = new QuestionListAdapter(this, new ArrayList<>(), new TypedGenericCallback<ArrayList<QuestionAnswer>>() {
            @Override
            public void onSuccess(ArrayList<QuestionAnswer> answers) {
                SurveyCorrespondentResponse surveyResponse = new SurveyCorrespondentResponse();
                surveyResponse.setSurveyTitle(survey.getTitle());
                surveyResponse.setSurveyDescription(survey.getDescription());
                surveyResponse.setName(nameTextInput.getText().toString());
                surveyResponse.setAge(Integer.parseInt(ageTextInput.getText().toString()));
                presenter.onSaveAnswers(surveyResponse, answers);
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(SurveyFormView.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }, new TypedGenericCallback<Integer>() {
            @Override
            public void onSuccess(Integer position) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                questionListAdapter.setCurrentCameraPos(position);
                cameraResultLauncher.launch(cameraIntent);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

        questionList.setAdapter(questionListAdapter);
        questionList.setLayoutManager(new LinearLayoutManager(this));

        Intent intent = getIntent();

        presenter.loadSurvey(intent.getIntExtra("page", 1));

        saveFab.setOnClickListener(v -> {
            questionListAdapter.saveAnswers();
        });

    }

    @Override
    public void renderSurvey(Survey survey) {
        surveyTitle.setText(survey.getTitle());
        questionListAdapter.setQuestions(survey.getQuestions());
        this.survey = survey;
        loader.setVisibility(View.GONE);
    }

    @Override
    public void redirect(int responseId) {
        runOnUiThread(() -> {
            createNotificationChannel();
            Intent intent = new Intent(this, ResponseDetailView.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra("id", responseId);

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.baseline_edit_note_24)
                    .setContentTitle("New Survey Response")
                    .setContentText("Go to response detail?")
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

            if (ActivityCompat.checkSelfPermission(
                            this,
                    Manifest.permission.POST_NOTIFICATIONS
               ) != PackageManager.PERMISSION_GRANTED
           ){
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.POST_NOTIFICATIONS},
                        PERMISSION_REQUEST_CODE);
            }else {
                notificationManager.notify(NOTIFICATION_ID, builder.build());
            }

            finish();

        });


    }

    @Override
    public void updateImagePath(String path, Bitmap bitmapImage) {
        questionListAdapter.setImagePath(path, bitmapImage);
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is not in the Support Library.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name =CHANNEL_ID;
            String description = "Basic channel";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this.
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void setAlertDialog(int responseId) {
        runOnUiThread(() -> {
           AlertDialog.Builder builder = new AlertDialog.Builder(SurveyFormView.this);

           builder.setTitle("View response?");

           builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
               @Override
               public void onClick(DialogInterface dialog, int which) {
                   Intent detailPageIntent = new Intent(SurveyFormView.this, ResponseDetailView.class);
                   detailPageIntent.putExtra("id", responseId);
                   startActivity(detailPageIntent);
                   finish();
               }
           });

           builder.setNegativeButton("Home", new DialogInterface.OnClickListener() {
               @Override
               public void onClick(DialogInterface dialog, int which) {
                   finish();
               }
           });

           builder.create().show();
       });

    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//
//        if (requestCode == PERMISSION_REQUEST_CODE) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                startLocationTrackingService();
//            } else {
//                // y want to disable location-related features or gracefully handle the lack of permission
//            }
//        }
//    }
}
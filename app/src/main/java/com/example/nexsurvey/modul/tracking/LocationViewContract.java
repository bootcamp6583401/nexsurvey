package com.example.nexsurvey.modul.tracking;

import java.util.ArrayList;

public interface LocationViewContract {

    void renderLocations(ArrayList<UserLocation> userLocations);
}

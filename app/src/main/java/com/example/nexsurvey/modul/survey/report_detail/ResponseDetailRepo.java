package com.example.nexsurvey.modul.survey.report_detail;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Looper;

import androidx.core.os.HandlerCompat;

import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.core.db.AppDatabase;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;

import java.io.File;
import java.util.List;
import java.util.concurrent.Executors;

public class ResponseDetailRepo implements ResponseDetailRepoContract{
    Context context;

    public ResponseDetailRepo(Context context) {
        this.context = context;
    }

    @Override
    public void getResponse(int id, TypedGenericCallback<SurveyCorrespondentResponse> callback) {
        Executors.newCachedThreadPool().execute(() -> {
            try {
                AppDatabase db = AppDatabase.getInstance(context);
                SurveyCorrespondentResponse response =  db.responseDao().loadById(id);

                List<QuestionAnswer> answers = db.answerDao().loadByResponseId(id);

                for(QuestionAnswer answer: answers){
                    if(answer.getImagePath() != null){
                        answer.setImageBitmap(loadImage(answer.getImagePath()));
                    }
                }

                response.setAnswers(answers);

                HandlerCompat.createAsync(Looper.getMainLooper()).post(() -> callback.onSuccess(response));

            }catch (Exception e){

            }
        });

    }

    public Bitmap loadImage(String imagePath) {
        try {
            File file = new File(imagePath);

            if (!file.exists()) {
                return null;
            }

            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

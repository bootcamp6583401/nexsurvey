package com.example.nexsurvey.modul.survey.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nexsurvey.R;
import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.modul.survey.AnswersValidator;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.example.nexsurvey.modul.survey.http.Question;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

public class QuestionListAdapter extends RecyclerView.Adapter<QuestionListAdapter.ViewHolder> {

    Context context;
    ArrayList<Question> questions;

    ArrayList<QuestionAnswer> answers = new ArrayList<>();

    TypedGenericCallback<ArrayList<QuestionAnswer>> onSaveCallback;
    TypedGenericCallback<Integer> onClickCamera;

    Integer currentCameraPos;

    public Integer getCurrentCameraPos() {
        return currentCameraPos;
    }

    public void setCurrentCameraPos(Integer currentCameraPos) {
        this.currentCameraPos = currentCameraPos;
    }


    public void setImagePath(String path, Bitmap bitmapImage) {
        questions.get(getCurrentCameraPos()).getAnswer().setImagePath(path);
        notifyItemChanged(getCurrentCameraPos(), bitmapImage);
    }

    public void setQuestions(List<Question> questions) {
        this.questions = new ArrayList<>(questions);

        for(Question question: questions){
            question.initAnswer();
            this.answers.add(new QuestionAnswer(question.getQuestion(), null, new ArrayList<>(), question.getRequired()));
        }
        notifyDataSetChanged();
    }

    public void saveAnswers(){
        AnswersValidator validator = new AnswersValidator(onSaveCallback);
        validator.validateAnswers(questions);
    }


    public QuestionListAdapter(Context context, ArrayList<Question> questions, TypedGenericCallback<ArrayList<QuestionAnswer>> callback, TypedGenericCallback<Integer> onClickCamera) {
        this.context = context;
        this.questions = questions;
        this.onSaveCallback = callback;
        this.onClickCamera = onClickCamera;
        for(Question question: questions){
            question.initAnswer();
            this.answers.add(new QuestionAnswer(question.getQuestion(), null, new ArrayList<>(), question.getRequired()));
        }
    }


    @NonNull
    @Override
    public QuestionListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater  = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.question_list_row, parent, false);
        return new QuestionListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if(!payloads.isEmpty()){

            if(payloads.get(0) instanceof Bitmap){
                holder.image.setImageBitmap((Bitmap) payloads.get(0));
            }
        }else {
            super.onBindViewHolder(holder, position, payloads);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionListAdapter.ViewHolder holder, int position) {
        Question question = questions.get(position);
        holder.questionTitle.setText(String.format("%d. %s%s", position + 1, question.getQuestion(), question.getRequired() ? " *" : ""));
        holder.checkboxList.setVisibility(View.GONE);
        holder.questionRadioGroup.setVisibility(View.GONE);
        holder.textInput.setVisibility(View.GONE);
        holder.textInputLayout.setVisibility(View.GONE);
        holder.dropdown.setVisibility(View.GONE);
        holder.image.setVisibility(View.GONE);
        holder.questionRadioGroup.removeAllViews();

        if(question.getRequired()){
            holder.questionTitle.setTextColor(ContextCompat.getColor(context, R.color.red));
        }


        // remove listener
        holder.questionRadioGroup.setOnCheckedChangeListener(null);
        holder.dropdown.setOnItemSelectedListener(null);
        holder.image.setOnClickListener(null);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Question question = questions.get(holder.getAdapterPosition());
                question.getAnswer().setSingleAnswer(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        };

        holder.textInput.removeTextChangedListener(textWatcher);


        if(question.getType().equals("checkbox")){
            holder.checkboxList.setVisibility(View.VISIBLE);
            ArrayAdapter<String> cbAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, question.getOptions()){

                @NonNull
                @Override
                public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    CheckBox checkBox;
                    if (convertView == null) {
                        checkBox = new CheckBox(getContext());
                    } else {
                        checkBox = (CheckBox) convertView;
                    }

                    checkBox.setText(getItem(position));

                    boolean checkedValue = question.getAnswer().getListAnswers().contains(getItem(position));

                    checkBox.setChecked(checkedValue);

                    checkBox.setOnCheckedChangeListener((v, checked ) -> {
                        if(checked) {
                            question.getAnswer().getListAnswers().add(getItem(position));
                        }else {
                            question.getAnswer().getListAnswers().remove(getItem(position));
                        }
                    });

                    return checkBox;
                }
            };

            holder.checkboxList.setAdapter(cbAdapter);

        } else if(question.getType().equals("radio")){
            holder.questionRadioGroup.setVisibility(View.VISIBLE);

            for(int i = 0; i < question.getOptions().size(); i++){
                RadioButton radioButton = new RadioButton(context);
                radioButton.setText(question.getOptions().get(i));
                radioButton.setId(3499+i);

                holder.questionRadioGroup.addView(radioButton);

                if(question.getAnswer().getSingleAnswer() != null && question.getAnswer().getSingleAnswer().equals(question.getOptions().get(i))){
                    radioButton.setChecked(true);
                }
            }
            holder.questionRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton checkedBtn = group.findViewById(checkedId);
                    question.getAnswer().setSingleAnswer(checkedBtn.getText().toString());
                }
            });
        }else if(question.getType().equals("dropdown")){
            holder.dropdown.setVisibility(View.VISIBLE);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, question.getOptions());

            holder.dropdown.setAdapter(adapter);


            for(int i= 0; i < question.getOptions().size(); i++){
                if(question.getAnswer().getSingleAnswer() !=null &&
                        question.getOptions().get(i).equals(question.getAnswer().getSingleAnswer())){
                    holder.dropdown.setSelection(i);
                }
            }

            holder.dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    question.getAnswer().setSingleAnswer(question.getOptions().get(position));
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    question.getAnswer().setSingleAnswer(null);
                }
            });
        }else if(question.getType().equals("camera")) {
            holder.image.setVisibility(View.VISIBLE);
            holder.image.setOnClickListener(v -> {
                onClickCamera.onSuccess(holder.getAdapterPosition());
            });

        }else {
            holder.textInput.setVisibility(View.VISIBLE);
            holder.textInputLayout.setVisibility(View.VISIBLE);
            holder.textInput.addTextChangedListener(textWatcher);
            holder.textInput.setText(question.getAnswer().getSingleAnswer());
        }

    }

    @Override
    public int getItemCount() {
        return answers.size();
    }

    public static class ViewHolder  extends RecyclerView.ViewHolder {
        TextView questionTitle;

        ListView checkboxList;

        TextInputEditText textInput;
        TextInputLayout textInputLayout;

        RadioGroup questionRadioGroup;

        ImageView image;

        Spinner dropdown;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            questionTitle = itemView.findViewById(R.id.tvQuestionTitle);
            checkboxList = itemView.findViewById(R.id.lvCheckboxList);
            questionRadioGroup = itemView.findViewById(R.id.rgQuestionRadio);
            textInput = itemView.findViewById(R.id.etSurveyFreeText);
            textInputLayout = itemView.findViewById(R.id.tilSurveyFreeText);
            dropdown = itemView.findViewById(R.id.spnDropdown);
            image = itemView.findViewById(R.id.ivImage);
        }
    }
}
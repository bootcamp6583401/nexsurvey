package com.example.nexsurvey.modul.survey.report.dashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nexsurvey.R;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;

import java.util.ArrayList;

public class DashboardView extends AppCompatActivity implements DashboardViewContract{

    ImageView backBtn;

    TextView currentUserText, totalResponsesText;

    RecyclerView listQuestionsList;

    DashboardPresenterContract presenter;

    ListAnswerAdapter listAnswerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_view);

        presenter = new DashboardPresenter(this, new DashboardRepo(this));

        listQuestionsList = findViewById(R.id.rvListQuestionsSummaries);

        currentUserText = findViewById(R.id.tvCurrentUserValue);
        totalResponsesText = findViewById(R.id.tvTotalResponseValue);

        listAnswerAdapter = new ListAnswerAdapter(this , new ArrayList<>());

        listQuestionsList.setAdapter(listAnswerAdapter);
        listQuestionsList.setLayoutManager( new LinearLayoutManager(this));

        presenter.getDashboardData();

        backBtn = findViewById(R.id.btnBack);

        backBtn.setOnClickListener( v -> {
            finish();
        });
    }

    @Override
    public void renderDashboard(ArrayList<QuestionAnswer> answers, int totalResponses, String currentUser, ArrayList<Summary> summaries) {
        currentUserText.setText(currentUser);
        totalResponsesText.setText(Integer.toString(totalResponses));
        listAnswerAdapter.setSummaries(summaries);
    }
}
package com.example.nexsurvey.modul.survey.report_detail;

import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;

public interface ResponseDetailRepoContract {

    void getResponse(int id, TypedGenericCallback<SurveyCorrespondentResponse> callback);
}

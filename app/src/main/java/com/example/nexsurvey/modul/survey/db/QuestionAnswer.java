package com.example.nexsurvey.modul.survey.db;

import android.graphics.Bitmap;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.ArrayList;
import java.util.List;

@Entity(tableName = "survey_answers")
public class QuestionAnswer {

    @PrimaryKey(autoGenerate = true)
    private Integer id;

    private Integer responseId;
    String question;

    ArrayList<String> listAnswers;

    String singleAnswer;

    String imagePath;

    boolean required;

    @Ignore
    Bitmap imageBitmap;

    public QuestionAnswer(String question, String singleAnswer, ArrayList<String> listAnswers, boolean required) {
        this.question = question;
        this.listAnswers = listAnswers;
        this.singleAnswer = singleAnswer;
        this.required = true;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<String> getListAnswers() {
        return listAnswers;
    }

    public void setListAnswers(ArrayList<String> listAnswers) {
        this.listAnswers = listAnswers;
    }

    public String getSingleAnswer() {
        return singleAnswer;
    }

    public void setSingleAnswer(String singleAnswer) {
        this.singleAnswer = singleAnswer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getResponseId() {
        return responseId;
    }

    public void setResponseId(Integer responseId) {
        this.responseId = responseId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Bitmap getImageBitmap() {
        return imageBitmap;
    }

    public void setImageBitmap(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}

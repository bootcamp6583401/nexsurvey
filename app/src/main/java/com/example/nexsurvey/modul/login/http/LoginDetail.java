package com.example.nexsurvey.modul.login.http;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginDetail {

    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Gander")
    @Expose
    private String gander;
    @SerializedName("Age")
    @Expose
    private Integer age;
    @SerializedName("Role")
    @Expose
    private String role;
    @SerializedName("Company")
    @Expose
    private String company;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGander() {
        return gander;
    }

    public void setGander(String gander) {
        this.gander = gander;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}

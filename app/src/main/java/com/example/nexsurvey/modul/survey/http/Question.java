package com.example.nexsurvey.modul.survey.http;

import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Question {
    @SerializedName("jenis")
    @Expose
    private String type;
    @SerializedName("pertanyaan")
    @Expose
    private String question;
    @SerializedName("opsi")
    @Expose
    private List<String> options;
    @SerializedName("wajib")
    @Expose
    private Boolean required;

    private QuestionAnswer answer;

    public QuestionAnswer getAnswer() {
        return answer;
    }

    public void setAnswer(QuestionAnswer answer) {
        this.answer = answer;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public void initAnswer(){
        this.answer = new QuestionAnswer(this.getQuestion(), null, new ArrayList<>(), false);
    }
}

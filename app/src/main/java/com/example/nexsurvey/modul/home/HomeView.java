package com.example.nexsurvey.modul.home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.example.nexsurvey.R;
import com.example.nexsurvey.modul.survey.SurveyFormView;
import com.example.nexsurvey.modul.survey.http.Survey;
import com.example.nexsurvey.modul.survey.report.SurveyReportView;
import com.example.nexsurvey.modul.tracking.LocationView;

import java.util.ArrayList;

public class HomeView extends AppCompatActivity implements HomeViewContract {

    Button surveyBtn, reportBtn;

    CardView csSurveyCard, osSurveyCard, surveyResponseCard, locationReportCard;

    TextView homeTitle;

    RecyclerView surveysList;

    SurveyListAdapter surveyListAdapter;

    HomePresenterContract presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_view);

        presenter = new HomePresenter(this, new HomeRepo(this));

        surveyBtn = findViewById(R.id.btnSurvey);
        reportBtn = findViewById(R.id.btnReport);

        csSurveyCard = findViewById(R.id.cardSatisfactionSurvey);
        osSurveyCard = findViewById(R.id.cardProductSurvey);

        locationReportCard = findViewById(R.id.cvLocationReport);
        surveyResponseCard = findViewById(R.id.cvSurveyReport);

        surveysList = findViewById(R.id.rvSurveys);

        homeTitle = findViewById(R.id.tvHomeTitle);


        SharedPreferences sharedPreferences = getSharedPreferences("nex_survey", Context.MODE_PRIVATE);

        String username = sharedPreferences.getString("username", null);

        homeTitle.setText("Hey, " + username + "!");

        csSurveyCard.setOnClickListener(v -> {
            Intent surveyPageIntent = new Intent(HomeView.this, SurveyFormView.class);
            surveyPageIntent.putExtra("page", 1);
            startActivity(surveyPageIntent);
        });

        osSurveyCard.setOnClickListener(v -> {
            Intent surveyPageIntent = new Intent(HomeView.this, SurveyFormView.class);
            surveyPageIntent.putExtra("page", 2);
            startActivity(surveyPageIntent);
        });

        surveyResponseCard.setOnClickListener(v -> {
            Intent surveyReportPageIntent = new Intent(HomeView.this, SurveyReportView.class);
            startActivity(surveyReportPageIntent);
        });

        locationReportCard.setOnClickListener(v -> {
            Intent locationReportIntent = new Intent(HomeView.this, LocationView.class);
            startActivity(locationReportIntent);
        });

        surveyListAdapter = new SurveyListAdapter(this, this ,new ArrayList<>());
        surveysList.setAdapter(surveyListAdapter);
        surveysList.setLayoutManager(new LinearLayoutManager(this));

        presenter.loadSurveys();
    }

    @Override
    public void renderSurveys(ArrayList<Survey> surveys) {
        surveyListAdapter.setSurveys(surveys);
    }

    @Override
    public void goToSurveyDetail(int page) {
        Intent surveyPageIntent = new Intent(HomeView.this, SurveyFormView.class);
        surveyPageIntent.putExtra("page", page);
        startActivity(surveyPageIntent);
    }

}
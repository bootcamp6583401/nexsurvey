package com.example.nexsurvey.modul.login.http;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponseData {

    @SerializedName("detail")
    @Expose
    private LoginDetail detail;
    @SerializedName("token")
    @Expose
    private String token;

    public LoginDetail getLoginDetail() {
        return detail;
    }

    public void setLoginDetail(LoginDetail detail) {
        this.detail = detail;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
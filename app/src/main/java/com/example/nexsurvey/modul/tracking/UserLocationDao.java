package com.example.nexsurvey.modul.tracking;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Upsert;


import java.util.ArrayList;
import java.util.List;

@Dao
public interface UserLocationDao {
    @Query("SELECT * FROM user_locations")
    List<UserLocation> getAll();

    @Query("SELECT * FROM user_locations where id = :id")
    UserLocation loadById(int id);

    @Query("SELECT * FROM user_locations ORDER BY id DESC LIMIT 1")
    UserLocation loadLatest();

    @Insert
    void insertAll(ArrayList<UserLocation> answers);

    @Insert
    long insert(UserLocation questionAnswer);

    @Delete
    void delete(UserLocation questionAnswer);

    @Upsert
    void upsert(List<UserLocation> questionAnswers);

}
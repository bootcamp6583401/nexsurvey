package com.example.nexsurvey.modul.home;

import com.example.nexsurvey.modul.survey.http.Survey;

import java.util.ArrayList;

public interface HomeViewContract {
    void renderSurveys(ArrayList<Survey> surveys);

    void goToSurveyDetail(int page);
}

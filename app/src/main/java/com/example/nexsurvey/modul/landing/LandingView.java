package com.example.nexsurvey.modul.landing;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Button;

import com.example.nexsurvey.R;
import com.example.nexsurvey.modul.login.LoginView;
import com.example.nexsurvey.services.LocationTrackerService;

public class LandingView extends AppCompatActivity {

    Button startBtn;

    private static final int PERMISSION_REQUEST_CODE = 1001;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_view);

        startBtn = findViewById(R.id.btnStart);

        startBtn.setOnClickListener(v -> {
            redirectToLogin();
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Request the permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_CODE);
        } else {
            // Permission has already been granted
            startLocationTrackingService();
        }

    }

    protected void redirectToLogin(){
        Intent loginIntent = new Intent(LandingView.this, LoginView.class);
        startActivity(loginIntent);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startLocationTrackingService();
            } else {
                // y want to disable location-related features or gracefully handle the lack of permission
            }
        }
    }

    private void startLocationTrackingService() {
        startService(new Intent(this, LocationTrackerService.class));

    }


}
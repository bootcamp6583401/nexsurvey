package com.example.nexsurvey.modul.survey.report_detail;

public interface ResponseDetailPresenterContract {

    void loadResponse(int id);
}

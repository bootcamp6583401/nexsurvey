package com.example.nexsurvey.modul.survey.http;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SurveyListResponse {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("data")
    @Expose
    private List<SurveyListData> data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<SurveyListData> getData() {
        return data;
    }

    public void setData(List<SurveyListData> data) {
        this.data = data;
    }

}

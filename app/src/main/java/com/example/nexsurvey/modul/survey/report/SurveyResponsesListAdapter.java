package com.example.nexsurvey.modul.survey.report;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nexsurvey.R;
import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.modul.survey.SurveyResponse;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;
import com.example.nexsurvey.modul.survey.http.Question;

import java.util.ArrayList;
import java.util.List;

public class SurveyResponsesListAdapter extends RecyclerView.Adapter<SurveyResponsesListAdapter.ViewHolder> {

    Context context;
    ArrayList<SurveyCorrespondentResponse> responses;

    TypedGenericCallback<Integer> onClickRow;


    public void setResponses(List<SurveyCorrespondentResponse> responses) {
        this.responses = new ArrayList<>(responses);

        notifyDataSetChanged();
    }


    public SurveyResponsesListAdapter(Context context, ArrayList<SurveyCorrespondentResponse> responses, TypedGenericCallback<Integer> onClickRow) {
        this.context = context;
        this.responses = responses;
        this.onClickRow = onClickRow;

    }


    @NonNull
    @Override
    public SurveyResponsesListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater  = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.survey_response_row, parent, false);
        return new SurveyResponsesListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SurveyResponsesListAdapter.ViewHolder holder, int position) {
       holder.surveyTitle.setText(responses.get(position).getSurveyTitle());
       holder.correspondentName.setText(responses.get(position).getName());
       holder.correspondentAge.setText(String.format("%d yrs", responses.get(position).getAge()));

       holder.viewBtn.setOnClickListener(v -> {
           onClickRow.onSuccess(responses.get(position).getId());
       });
    }

    @Override
    public int getItemCount() {
        return responses.size();
    }

    public static class ViewHolder  extends RecyclerView.ViewHolder {
        TextView surveyTitle, correspondentName, correspondentAge;

        Button viewBtn;

        RelativeLayout row;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            surveyTitle = itemView.findViewById(R.id.tvSurveyTitle);
            correspondentName = itemView.findViewById(R.id.tvCorrespondentName);
            correspondentAge = itemView.findViewById(R.id.tvCorrespondentAge);
            viewBtn = itemView.findViewById(R.id.btnView);

            row = itemView.findViewById(R.id.rlResponseRow);
        }
    }
}
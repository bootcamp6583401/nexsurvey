package com.example.nexsurvey.modul.survey;

import android.graphics.Bitmap;

import com.example.nexsurvey.modul.survey.http.Survey;

public interface SurveyFormViewContract {
    void renderSurvey(Survey survey);

    void redirect(int responseId);

    void updateImagePath(String path, Bitmap bitmap);
}

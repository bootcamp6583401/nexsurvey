package com.example.nexsurvey.modul.home;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nexsurvey.R;
import com.example.nexsurvey.modul.survey.http.Survey;

import java.util.ArrayList;

public class SurveyListAdapter extends RecyclerView.Adapter<SurveyListAdapter.ViewHolder> {
    Context context;

    HomeViewContract view;

    ArrayList<Survey> surveys;

    public void setSurveys(ArrayList<Survey> surveys){
        this.surveys = surveys;
        notifyDataSetChanged();
    }

    public SurveyListAdapter(Context context, HomeViewContract view, ArrayList<Survey> surveys) {
        this.context = context;
        this.surveys = surveys;
        this.view = view;
    }

    @NonNull
    @Override
    public SurveyListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.survey_row, parent, false);
        return new SurveyListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SurveyListAdapter.ViewHolder holder, int position) {

        holder.surveyTitle.setText(surveys.get(position).getTitle());
        holder.surveyDesc.setText(surveys.get(position).getDescription());

        holder.parent.setOnClickListener(v -> {
            view.goToSurveyDetail(position + 1);
        });

    }

    @Override
    public int getItemCount() {
        return surveys.size();
    }

    public static class ViewHolder extends  RecyclerView.ViewHolder {
        TextView surveyTitle, surveyDesc;

        CardView parent;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            surveyTitle = itemView.findViewById(R.id.tvSurveyTitle);
            surveyDesc = itemView.findViewById(R.id.tvSurveyDescription);
            parent = itemView.findViewById(R.id.cvSurveyRow);


        }
    }
}

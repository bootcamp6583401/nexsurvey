package com.example.nexsurvey.modul.login;

import com.example.nexsurvey.modul.login.http.LoginRequest;

public interface LoginRepoContract {
    void login(LoginRequest loginRequest, LoginCallback loginCallback);
}

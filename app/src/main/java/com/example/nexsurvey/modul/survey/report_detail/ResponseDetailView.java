package com.example.nexsurvey.modul.survey.report_detail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nexsurvey.R;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;

import java.util.ArrayList;

public class ResponseDetailView extends AppCompatActivity implements ResponseDetailViewContract{

    RecyclerView answersList;

    ResponseDetailPresenterContract presenter;

    AnswersListAdapter answersListAdapter;

    ImageView backBtn;

    TextView correspondentName, correspondentAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.response_detail_view);

        answersList = findViewById(R.id.rvAnswers);

        backBtn = findViewById(R.id.btnBack);

        correspondentAge = findViewById(R.id.tvDetailAge);
        correspondentName = findViewById(R.id.tvDetailName);

        presenter = new ResponseDetailPresenter(this, new ResponseDetailRepo(this));

        answersListAdapter = new AnswersListAdapter(this, new ArrayList<>());
        answersList.setAdapter(answersListAdapter);
        answersList.setLayoutManager(new LinearLayoutManager(this));

        Intent intent = getIntent();
        presenter.loadResponse(intent.getIntExtra("id", 1));

        backBtn.setOnClickListener(v -> finish());

    }

    @Override
    public void renderResponse(SurveyCorrespondentResponse response) {
        answersListAdapter.setAnswers(new ArrayList<>(response.getAnswers()));

        correspondentAge.setText(String.format("%d", response.getAge()));
        correspondentName.setText(response.getName());

    }
}
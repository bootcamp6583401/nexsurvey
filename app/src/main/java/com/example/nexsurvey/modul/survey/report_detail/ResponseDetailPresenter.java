package com.example.nexsurvey.modul.survey.report_detail;

import android.content.Context;

import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;

public class ResponseDetailPresenter implements ResponseDetailPresenterContract{
    ResponseDetailViewContract view;

    ResponseDetailRepoContract repo;

    public ResponseDetailPresenter(ResponseDetailViewContract view, ResponseDetailRepoContract repo) {
        this.view = view;
        this.repo = repo;
    }

    @Override
    public void loadResponse(int id) {
        repo.getResponse(id, new TypedGenericCallback<SurveyCorrespondentResponse>() {
            @Override
            public void onSuccess(SurveyCorrespondentResponse object) {
                view.renderResponse(object);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
}

package com.example.nexsurvey.modul.login.http;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginErrorResponse {
    @SerializedName("code")
    @Expose
    private Integer code;

    @SerializedName("description")
    @Expose
    private String description;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LoginErrorResponse(Integer code, String description) {
        this.code = code;
        this.description = description;
    }
}

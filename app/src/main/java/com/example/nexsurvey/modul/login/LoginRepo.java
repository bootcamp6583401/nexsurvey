package com.example.nexsurvey.modul.login;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.nexsurvey.core.RetrofitClient;
import com.example.nexsurvey.modul.login.http.LoginApi;
import com.example.nexsurvey.modul.login.http.LoginErrorResponse;
import com.example.nexsurvey.modul.login.http.LoginRequest;
import com.example.nexsurvey.modul.login.http.LoginResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRepo implements  LoginRepoContract{

    LoginApi loginApi;

    Context context;

    public LoginRepo(Context context) {
        this.context = context;
        this.loginApi = RetrofitClient.getClient(context).create(LoginApi.class);
    }

    @Override
    public void login(LoginRequest loginRequest, LoginCallback loginCallback) {
        Call<LoginResponse> call = loginApi.login(loginRequest);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                LoginResponseHandler loginResponseHandler = new LoginResponseHandler(loginCallback, context);
                loginResponseHandler.handleResponse(response);
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponse> call, Throwable t) {
                // Handle failure
                String testMsg = t.getMessage();
                loginCallback.onFailure("Something went wrong.");

            }
        });
    }
}

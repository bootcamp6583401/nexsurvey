package com.example.nexsurvey.modul.survey.report.dashboard;

import com.example.nexsurvey.core.TypedGenericCallback;

import java.util.Map;

public interface DashboardPresenterContract {

    void getDashboardData();
}

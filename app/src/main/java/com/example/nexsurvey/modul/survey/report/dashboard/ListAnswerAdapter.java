package com.example.nexsurvey.modul.survey.report.dashboard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nexsurvey.R;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.example.nexsurvey.modul.survey.report_detail.AnswersListAdapter;

import java.util.ArrayList;

public class ListAnswerAdapter extends RecyclerView.Adapter<ListAnswerAdapter.ViewHolder>{
    Context context;

    ArrayList<Summary> summaries;

    public void setSummaries(ArrayList<Summary> summaries){
        this.summaries = summaries;
        notifyDataSetChanged();
    }

    public ListAnswerAdapter(Context context, ArrayList<Summary> summaries) {
        this.context = context;
        this.summaries = summaries;
    }

    @NonNull
    @Override
    public ListAnswerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.dashboard_list_row, parent, false);
        return new ListAnswerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListAnswerAdapter.ViewHolder holder, int position) {

        Summary summary = summaries.get(position);
        holder.questionText.setText(summary.getQuestionText());

        ArrayAdapter<SummaryItem> adapter = new ArrayAdapter<SummaryItem>(context, android.R.layout.simple_list_item_1, summary.getSummaryItems()){

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

                TextView answerText, answerTotal;

                if (convertView == null) {
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.dashboard_list_answer_row, parent, false);

                }

                answerText = convertView.findViewById(R.id.tvAnswerText);
                answerTotal = convertView.findViewById(R.id.tvAnswerTotal);


                answerTotal.setText(Integer.toString(summary.getSummaryItems().get(position).getTotal()));
                answerText.setText(summary.getSummaryItems().get(position).getAnswerText());

                return convertView;
            }
        };

        holder.multipleAnswersList.setAdapter(adapter);

    }

    @Override
    public int getItemCount() {
        return summaries.size();
    }

    public static class ViewHolder extends  RecyclerView.ViewHolder {
        TextView questionText;

        ListView multipleAnswersList;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            questionText = itemView.findViewById(R.id.tvQuestionText);
            multipleAnswersList = itemView.findViewById(R.id.lvMultipleAnswers);
        }
    }
}

package com.example.nexsurvey.modul.survey.report;

import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;

import java.util.ArrayList;

public interface SurveyReportRepoContract {
    void getResponses(TypedGenericCallback<ArrayList<SurveyCorrespondentResponse>> callback);

}

package com.example.nexsurvey.modul.survey;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import com.example.nexsurvey.core.GenericCallback;
import com.example.nexsurvey.core.RetrofitClient;
import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.core.db.AppDatabase;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;
import com.example.nexsurvey.modul.survey.http.SurveyApi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SurveyFormRepo implements  SurveyFormRepoContract{
    SurveyApi api;

    Context context;

    public SurveyFormRepo(Context context) {
        this.api = RetrofitClient.getClient(context).create(SurveyApi.class);
        this.context = context;

    }

    @Override
    public void getSurvey(int page, GenericCallback genericCallback) {
        Call<SurveyResponse> call = api.getOneSurvey(context.getSharedPreferences("nex_survey", Context.MODE_PRIVATE).getString("token", null), page);

        call.enqueue(new Callback<SurveyResponse>() {
            @Override
            public void onResponse(Call<SurveyResponse> call, Response<SurveyResponse> response) {
                if (response.isSuccessful()) {
                    SurveyResponse survey = response.body();

                    genericCallback.onSuccess(survey.getSurvey());
                } else {
                    // Handle error
                }
            }

            @Override
            public void onFailure(Call<SurveyResponse> call, Throwable t) {
                // Handle failure
            }
        });
    }

    @Override
    public void saveAnswers(SurveyCorrespondentResponse surveyResponse, ArrayList<QuestionAnswer> questionAnswers, GenericCallback genericCallback) {
       Thread thread = new Thread(() -> {

           long newResponseId = AppDatabase.getInstance(context).responseDao().insert(surveyResponse);
           for (QuestionAnswer answer :
                   questionAnswers) {
               answer.setResponseId((int)newResponseId);
           }
           AppDatabase.getInstance(context).answerDao().insertAll(questionAnswers);

           genericCallback.onSuccess(newResponseId);
       });

       thread.start();
    }

    @Override
    public void saveImage(Bitmap photo, TypedGenericCallback<String> onSavedCallback) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String fileName = "IMG_" + timeStamp + ".jpg";

        File directory = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File file = new File(directory, fileName);

        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            photo.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();
            onSavedCallback.onSuccess(file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    @Override
    public Bitmap loadImage(String imagePath) {
        try {
            File file = new File(imagePath);

            if (!file.exists()) {
                return null;
            }

            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

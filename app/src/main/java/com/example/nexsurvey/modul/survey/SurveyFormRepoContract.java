package com.example.nexsurvey.modul.survey;

import android.graphics.Bitmap;

import com.example.nexsurvey.core.GenericCallback;
import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;

import java.util.ArrayList;

public interface SurveyFormRepoContract {
    void getSurvey(int page, GenericCallback genericCallback);

    void saveAnswers(SurveyCorrespondentResponse surveyResponse, ArrayList<QuestionAnswer> answers, GenericCallback genericCallback);

    void saveImage(Bitmap photo, TypedGenericCallback<String> onSavedCallback);

    Bitmap loadImage(String imagePath);
}

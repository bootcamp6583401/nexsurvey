package com.example.nexsurvey.modul.survey;

import com.example.nexsurvey.modul.survey.http.Survey;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SurveyResponse {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("data")
    @Expose
    private Survey survey;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }
}

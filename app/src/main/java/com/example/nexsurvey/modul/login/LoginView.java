package com.example.nexsurvey.modul.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.nexsurvey.R;
import com.example.nexsurvey.modul.home.HomeView;
import com.example.nexsurvey.modul.login.http.LoginRequest;
import com.google.android.material.textfield.TextInputEditText;

public class LoginView extends AppCompatActivity implements LoginViewContract{

    LoginPresenterContract loginPresenter;

    // UI
    Button loginBtn;

    TextInputEditText usernameInput, passwordInput;

    RelativeLayout loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_view);

        loginBtn = findViewById(R.id.btnLogin);
        usernameInput = findViewById(R.id.etUsername);
        passwordInput = findViewById(R.id.etPassword);
        loading = findViewById(R.id.rlLoadingBg);

        loginPresenter = new LoginPresenter(this, new LoginRepo(this));

//        redirect();

        loginBtn.setOnClickListener(v -> {
            loginBtn.setEnabled(false);
            loading.setVisibility(View.VISIBLE);
            LoginRequest loginRequest = new LoginRequest(usernameInput.getText().toString(), passwordInput.getText().toString());
            loginPresenter.onLogin(loginRequest);
        });
    }

    @Override
    public void redirect() {
        Intent homeIntent = new Intent(LoginView.this, HomeView.class);
        startActivity(homeIntent);
        Toast.makeText(this, "Login Successful", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayError(String description) {
        loginBtn.setEnabled(true);
        loading.setVisibility(View.GONE);
        Toast.makeText(this, description, Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onResume() {
        loginBtn.setEnabled(true);
        loading.setVisibility(View.GONE);
        super.onResume();
    }
}
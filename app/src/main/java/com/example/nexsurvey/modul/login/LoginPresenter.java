package com.example.nexsurvey.modul.login;


import com.example.nexsurvey.modul.login.http.LoginRequest;
import com.example.nexsurvey.modul.login.http.LoginResponse;

public class LoginPresenter implements  LoginPresenterContract{

    LoginRepoContract loginRepo;

    LoginViewContract loginView;

    public LoginPresenter(LoginViewContract loginView, LoginRepoContract repo){
        this.loginRepo = repo;
        this.loginView = loginView;
    }
    @Override
    public void onLogin(LoginRequest loginRequest) {
        loginRepo.login(loginRequest, new LoginCallback() {
            @Override
            public void onSuccess(LoginResponse loginResponse) {
                loginView.redirect();
            }

            @Override
            public void onFailure(String description) {
                loginView.displayError(description);
            }
        });
    }
}

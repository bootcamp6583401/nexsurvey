package com.example.nexsurvey.modul.survey;

import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.example.nexsurvey.modul.survey.http.Question;

import java.util.ArrayList;

public class AnswersValidator {

    TypedGenericCallback<ArrayList<QuestionAnswer>> onSaveCallback;

    public AnswersValidator(TypedGenericCallback<ArrayList<QuestionAnswer>> onSaveCallback) {
        this.onSaveCallback = onSaveCallback;
    }

    public void validateAnswers(ArrayList<Question> questions) {
        ArrayList<QuestionAnswer> answers = new ArrayList<>();
        Boolean hasMissingAnswers = false;
        int errorPosition = -1;
        for(int i = 0; i < questions.size(); i++){
            Question question = questions.get(i);
            QuestionAnswer answer = question.getAnswer();
            if(errorPosition == -1 && (question.getRequired()
                    && (answer.getListAnswers().size() == 0 && (answer.getSingleAnswer() == null || answer.getSingleAnswer().length() ==0) && answer.getImagePath() == null))){
                hasMissingAnswers = true;
                errorPosition = i;
            }
            answers.add(answer);
        }

        if(hasMissingAnswers){
            onSaveCallback.onFailure(new Throwable(String.format("Question %d is missing an answer!", errorPosition+1)));
        }else {
            onSaveCallback.onSuccess(answers);
        }
    }
}

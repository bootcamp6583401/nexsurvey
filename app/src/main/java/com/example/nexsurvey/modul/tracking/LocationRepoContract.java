package com.example.nexsurvey.modul.tracking;

import com.example.nexsurvey.core.TypedGenericCallback;

import java.util.ArrayList;

public interface LocationRepoContract {

    void getUserLocations(TypedGenericCallback<ArrayList<UserLocation>> userLocationsCallback);
}

package com.example.nexsurvey.modul.home;

import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.modul.survey.http.Survey;

import java.util.ArrayList;

public interface HomeRepoContract {
    void getSurveys(TypedGenericCallback<ArrayList<Survey>> callback);
}

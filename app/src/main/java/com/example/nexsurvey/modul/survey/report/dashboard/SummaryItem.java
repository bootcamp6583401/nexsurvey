package com.example.nexsurvey.modul.survey.report.dashboard;

public class SummaryItem {

    private String answerText;
    private int total;

    public SummaryItem(String answerText, int total) {
        this.answerText = answerText;
        this.total = total;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}

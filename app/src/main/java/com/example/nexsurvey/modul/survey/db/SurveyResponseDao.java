package com.example.nexsurvey.modul.survey.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Upsert;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface SurveyResponseDao {
    @Query("SELECT * FROM survey_responses")
    List<SurveyCorrespondentResponse> getAll();

    @Query("SELECT * FROM survey_responses where id = :id")
    SurveyCorrespondentResponse loadById(int id);

    @Insert
    void insertAll(ArrayList<SurveyCorrespondentResponse> responses);

    @Insert
    long insert(SurveyCorrespondentResponse response);

    @Delete
    void delete(SurveyCorrespondentResponse response);

    @Upsert
    void upsert(List<SurveyCorrespondentResponse> responses);

    @Query("SELECT count(*) from survey_responses")
    int getTotal();
}

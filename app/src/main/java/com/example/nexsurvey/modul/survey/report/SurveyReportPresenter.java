package com.example.nexsurvey.modul.survey.report;

import android.widget.ImageView;

import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.modul.survey.SurveyFormRepoContract;
import com.example.nexsurvey.modul.survey.SurveyFormViewContract;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;

import java.util.ArrayList;

public class SurveyReportPresenter implements SurveyReportPresenterContract{
    SurveyReportRepoContract repo;
    SurveyReportViewContract view;


    public SurveyReportPresenter(SurveyReportViewContract view, SurveyReportRepoContract repo){
        this.repo = repo;
        this.view = view;
    }
    @Override
    public void loadResponses() {
        repo.getResponses(new TypedGenericCallback<ArrayList<SurveyCorrespondentResponse>>() {
            @Override
            public void onSuccess(ArrayList<SurveyCorrespondentResponse> object) {
                view.renderResponses(object);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
}

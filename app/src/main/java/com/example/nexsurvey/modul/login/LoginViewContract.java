package com.example.nexsurvey.modul.login;

public interface LoginViewContract {

    void redirect();

    void displayError(String description);
}

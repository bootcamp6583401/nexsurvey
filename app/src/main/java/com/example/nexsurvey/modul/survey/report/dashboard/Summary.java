package com.example.nexsurvey.modul.survey.report.dashboard;

import java.util.ArrayList;

public class Summary {

    private String questionText;
    private ArrayList<SummaryItem> summaryItems;

    public Summary(String questionText, ArrayList<SummaryItem> summaryItems) {
        this.questionText = questionText;
        this.summaryItems = summaryItems;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public ArrayList<SummaryItem> getSummaryItems() {
        return summaryItems;
    }

    public void setSummaryItems(ArrayList<SummaryItem> summaryItems) {
        this.summaryItems = summaryItems;
    }
}

package com.example.nexsurvey.modul.survey.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Upsert;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface QuestionAnswerDao {
    @Query("SELECT * FROM survey_answers")
    List<QuestionAnswer> getAll();

    @Query("SELECT * FROM survey_answers where id = :id")
    QuestionAnswer loadById(int id);

    @Query("SELECT * FROM survey_answers where responseId = :id")
    List<QuestionAnswer> loadByResponseId(int id);

//    @Query("SELECT * FROM survey_responses LEFT JOIN survey_answers on survey_responses.id = survey_answers.responseId where id = 1 LIMIT 1")
//    SurveyCorrespondentResponse loadResponseWithAnswers();

    @Insert
    void insertAll(ArrayList<QuestionAnswer> answers);

    @Insert
    long insert(QuestionAnswer questionAnswer);

    @Delete
    void delete(QuestionAnswer questionAnswer);

    @Upsert
    void upsert(List<QuestionAnswer> questionAnswers);

    @Query("SELECT * FROM survey_answers where listAnswers is not null and listAnswers != '[]'")
    List<QuestionAnswer> getAllListResponses();


}
package com.example.nexsurvey.modul.survey.report_detail;

import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;

public interface ResponseDetailViewContract {

    void renderResponse(SurveyCorrespondentResponse response);
}

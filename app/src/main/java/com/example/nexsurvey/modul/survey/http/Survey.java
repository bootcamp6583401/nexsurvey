package com.example.nexsurvey.modul.survey.http;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Survey {
    @SerializedName("judul")
    @Expose
    private String title;
    @SerializedName("deskripsi")
    @Expose
    private String description;
    @SerializedName("pertanyaan")
    @Expose
    private List<Question> questions;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}

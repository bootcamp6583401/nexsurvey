package com.example.nexsurvey.modul.login;

import com.example.nexsurvey.modul.login.http.LoginResponse;

public interface LoginCallback {
    void onSuccess(LoginResponse loginResponse);

    void onFailure(String description);
}

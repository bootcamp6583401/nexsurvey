package com.example.nexsurvey.modul.survey.http;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SurveyListData {
    @SerializedName("survei")
    @Expose
    private Survey survey;

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }
}

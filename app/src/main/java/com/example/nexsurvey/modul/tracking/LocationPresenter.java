package com.example.nexsurvey.modul.tracking;

import com.example.nexsurvey.core.TypedGenericCallback;

import java.util.ArrayList;

public class LocationPresenter implements  LocationPresenterContract{

    LocationViewContract view;

    LocationRepoContract repo;

    public LocationPresenter(LocationViewContract view, LocationRepoContract repo){
        this.view = view;
        this.repo = repo;

    }
    @Override
    public void loadUserLocations() {
        repo.getUserLocations(new TypedGenericCallback<ArrayList<UserLocation>>() {
            @Override
            public void onSuccess(ArrayList<UserLocation> object) {
                view.renderLocations(object);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
}

package com.example.nexsurvey.modul.survey.report;

import android.content.Context;
import android.os.Looper;

import androidx.core.os.HandlerCompat;

import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.core.db.AppDatabase;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

public class SurveyReportRepo implements  SurveyReportRepoContract {
    Context context;

    public SurveyReportRepo(Context context) {
        this.context = context;
    }

    @Override
    public void getResponses(TypedGenericCallback<ArrayList<SurveyCorrespondentResponse>> callback) {

        Executors.newCachedThreadPool().execute(() -> {
            try {
                AppDatabase db = AppDatabase.getInstance(context);
                List<SurveyCorrespondentResponse> responses =  db.responseDao().getAll();

                HandlerCompat.createAsync(Looper.getMainLooper()).post(() -> callback.onSuccess(new ArrayList<>(responses)));

            }catch (Exception e){

            }
        });


    }
}

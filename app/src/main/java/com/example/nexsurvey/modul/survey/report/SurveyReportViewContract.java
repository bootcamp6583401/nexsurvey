package com.example.nexsurvey.modul.survey.report;

import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;

import java.util.ArrayList;

public interface SurveyReportViewContract {
    void renderResponses(ArrayList<SurveyCorrespondentResponse> responses);
}

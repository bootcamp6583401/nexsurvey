package com.example.nexsurvey.modul.survey.report.dashboard;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.core.db.AppDatabase;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.example.nexsurvey.modul.survey.db.QuestionAnswerDao;
import com.example.nexsurvey.modul.survey.db.SurveyResponseDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DashboardRepo implements DashboardRepoContract{

    Context context;

    public DashboardRepo(Context context) {
        this.context = context;
    }

    @Override
    public void loadDashboardData(TypedGenericCallback<Map<String, Object>> callback) {
        new Thread(() -> {
            SurveyResponseDao surveyResponseDao = AppDatabase.getInstance(context).responseDao();
            QuestionAnswerDao questionAnswerDao = AppDatabase.getInstance(context).answerDao();

            int total = surveyResponseDao.getTotal();

            List<QuestionAnswer>  questionAnswers = questionAnswerDao.getAllListResponses();

            SharedPreferences sharedPreferences = context.getSharedPreferences("nex_survey", Context.MODE_PRIVATE);

            Map<String, Object> data = new HashMap<>();

            data.put("total", total);
            data.put("listAnswers", new ArrayList<>(questionAnswers));
            data.put("username", sharedPreferences.getString("username", "username"));

            Map<String, Map<String, Integer>> answerCounts = new HashMap<>();

            ArrayList<Summary> summaries = new ArrayList<>();

            for (QuestionAnswer answer : questionAnswers) {
                String questionText = answer.getQuestion();
                List<String> answerList = answer.getListAnswers();

                if (!answerCounts.containsKey(questionText)) {
                    answerCounts.put(questionText, new HashMap<>());
                }

                Map<String, Integer> counts = answerCounts.get(questionText);
                for (String ans : answerList) {
                    counts.put(ans, counts.getOrDefault(ans, 0) + 1);
                }
            }

            for (Map.Entry<String, Map<String, Integer>> entry : answerCounts.entrySet()) {
                String questionText = entry.getKey();
                List<SummaryItem> summaryItems = new ArrayList<>();
                Map<String, Integer> counts = entry.getValue();

                // Add SummaryItem objects
                for (Map.Entry<String, Integer> countEntry : counts.entrySet()) {
                    summaryItems.add(new SummaryItem(countEntry.getKey(), countEntry.getValue()));
                }

                summaries.add(new Summary(questionText, new ArrayList<>(summaryItems)));
            }

            data.put("summaries", summaries);


            callback.onSuccess(data);

        }).start();

    }
}

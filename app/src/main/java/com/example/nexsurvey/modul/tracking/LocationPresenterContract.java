package com.example.nexsurvey.modul.tracking;

public interface LocationPresenterContract {

    void loadUserLocations();
}

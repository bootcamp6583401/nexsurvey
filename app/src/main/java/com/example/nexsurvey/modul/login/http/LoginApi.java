package com.example.nexsurvey.modul.login.http;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface LoginApi {
    @Headers("Content-type: application/json")
    @POST("login")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);
}


package com.example.nexsurvey.modul.survey.report.dashboard;

import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;

import java.util.ArrayList;
import java.util.Map;

public class DashboardPresenter implements DashboardPresenterContract{

    DashboardViewContract view;
    DashboardRepoContract repo;

    public DashboardPresenter(DashboardViewContract view, DashboardRepoContract repo) {
        this.view = view;
        this.repo = repo;
    }

    @Override
    public void getDashboardData() {
        repo.loadDashboardData(new TypedGenericCallback<Map<String, Object>>() {
            @Override
            public void onSuccess(Map<String, Object> object) {
                ArrayList<QuestionAnswer> answers = (ArrayList<QuestionAnswer>) object.get("listAnswers");
                int total = (int) object.get("total");
                String username = (String) object.get("username");
                ArrayList<Summary> summaries = (ArrayList<Summary>) object.get("summaries");

                view.renderDashboard(answers, total, username, summaries);

            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
}

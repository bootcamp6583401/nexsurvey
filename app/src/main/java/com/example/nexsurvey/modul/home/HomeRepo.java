package com.example.nexsurvey.modul.home;

import android.content.Context;

import com.example.nexsurvey.core.RetrofitClient;
import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.modul.survey.SurveyResponse;
import com.example.nexsurvey.modul.survey.http.Survey;
import com.example.nexsurvey.modul.survey.http.SurveyApi;
import com.example.nexsurvey.modul.survey.http.SurveyListData;
import com.example.nexsurvey.modul.survey.http.SurveyListResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeRepo implements HomeRepoContract{
    SurveyApi api;

    Context context;

    public HomeRepo(Context context) {
        this.context = context;
        this.api = RetrofitClient.getClient(context).create(SurveyApi.class);

    }

    @Override
    public void getSurveys(TypedGenericCallback<ArrayList<Survey>> callback) {
        Call<SurveyListResponse> call =  api.getSurveyList(context.getSharedPreferences("nex_survey", Context.MODE_PRIVATE).getString("token", null));

        call.enqueue(new Callback<SurveyListResponse>() {
            @Override
            public void onResponse(Call<SurveyListResponse> call, Response<SurveyListResponse> response) {
                if (response.isSuccessful()) {
                    SurveyListResponse surveyListResponse = response.body();

                    ArrayList<Survey> surveys = new ArrayList<>();

                    for (SurveyListData surveyListData : surveyListResponse.getData()){
                        surveys.add(surveyListData.getSurvey());
                    }
                    callback.onSuccess(surveys);

                } else {
                    // Handle error
                }
            }

            @Override
            public void onFailure(Call<SurveyListResponse> call, Throwable t) {
                // Handle failure
            }
        });

    }
}

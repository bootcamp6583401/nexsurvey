package com.example.nexsurvey.modul.login;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.example.nexsurvey.modul.login.http.LoginErrorResponse;
import com.example.nexsurvey.modul.login.http.LoginResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import retrofit2.Response;

public class LoginResponseHandler {

    LoginCallback loginCallback;
    Context context;
    public LoginResponseHandler(LoginCallback loginCallback, Context context){
        this.loginCallback = loginCallback;
        this.context = context;
    }

    public void handleResponse(@NonNull Response<LoginResponse> loginResponseResponse){
        if (loginResponseResponse.isSuccessful()) {
            LoginResponse loginResponse = loginResponseResponse.body();

            if(loginResponse != null && loginResponse.getCode() == 200){
                SharedPreferences.Editor editor = context.getSharedPreferences("nex_survey", Context.MODE_PRIVATE).edit();

                editor.putString("username", loginResponse.getLoginResponseData().getLoginDetail().getName());
                editor.putString("token", loginResponse.getLoginResponseData().getToken());
                editor.commit();
                loginCallback.onSuccess(loginResponse);
            }else {
                loginCallback.onFailure("Something went wrong.");
            }
        } else {
            // Handle error
            Gson gson = new Gson();
            Type type = new TypeToken<LoginErrorResponse>() {}.getType();
            LoginErrorResponse errorResponse = gson.fromJson(loginResponseResponse.errorBody().charStream(),type);

            loginCallback.onFailure(errorResponse.getDescription() != null ? errorResponse.getDescription() : "Something went wrong.");
        }


    }
}

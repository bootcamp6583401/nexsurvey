package com.example.nexsurvey.modul.survey;

import android.graphics.Bitmap;

import com.example.nexsurvey.core.GenericCallback;
import com.example.nexsurvey.core.TypedGenericCallback;
import com.example.nexsurvey.modul.survey.db.QuestionAnswer;
import com.example.nexsurvey.modul.survey.db.SurveyCorrespondentResponse;
import com.example.nexsurvey.modul.survey.http.Survey;

import java.util.ArrayList;

public class SurveyFormPresenter implements  SurveyFormPresenterContract{
    SurveyFormRepoContract repo;
    SurveyFormViewContract view;

    public SurveyFormPresenter(SurveyFormViewContract view, SurveyFormRepoContract repo){
        this.repo = repo;
        this.view = view;
    }
    @Override
    public void loadSurvey(int page) {
        repo.getSurvey(page, new GenericCallback() {
            @Override
            public void onSuccess(Object object) {
                Survey survey = (Survey)object;
                view.renderSurvey(survey);
            }

            @Override
            public void onFailure(Object object) {

            }
        });
    }

    @Override
    public void onSaveAnswers(SurveyCorrespondentResponse surveyResponse, ArrayList<QuestionAnswer> answers) {
        repo.saveAnswers(surveyResponse, answers, new GenericCallback() {
            @Override
            public void onSuccess(Object object) {
                long test = 1;
                view.redirect((int) (long) object) ;
            }

            @Override
            public void onFailure(Object object) {

            }
        });
    }

    @Override
    public void saveImage(Bitmap imageBitmap) {
        repo.saveImage(imageBitmap, new TypedGenericCallback<String>() {
            @Override
            public void onSuccess(String object) {
                view.updateImagePath(object, imageBitmap);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
}

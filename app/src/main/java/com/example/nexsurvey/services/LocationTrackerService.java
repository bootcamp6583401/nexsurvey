package com.example.nexsurvey.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.nexsurvey.core.db.AppDatabase;
import com.example.nexsurvey.modul.tracking.UserLocation;
import com.example.nexsurvey.modul.tracking.UserLocationDao;

import java.util.Random;

public class LocationTrackerService extends Service {
    private LocationManager locationManager;
    private LocationListener locationListener;

    @Override
    public void onCreate() {
        super.onCreate();

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                new Thread(() -> {
                    UserLocationDao locationDao= AppDatabase.getInstance(getBaseContext()).locationDao();

                    UserLocation latest = locationDao.loadLatest();

                    UserLocation newUserLocation = new UserLocation(location.getLatitude(), location.getLongitude(), "ANSELL");


                    if(latest == null || ( latest.getLatitude() != newUserLocation.getLatitude() || latest.getLongitude() != newUserLocation.getLongitude())){
                        locationDao.insert(newUserLocation);
                    }
                }).start();
                Log.d("LocationTracking", "Latitude: " + location.getLatitude() + ", Longitude: " + location.getLongitude());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {}

            @Override
            public void onProviderEnabled(String provider) {}

            @Override
            public void onProviderDisabled(String provider) {}
        };

        try {
            // Request location updates every 5 seconds with a minimum distance change of 0 meters
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 60, 0, locationListener);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            try {
                locationManager.removeUpdates(locationListener);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
    }
}
